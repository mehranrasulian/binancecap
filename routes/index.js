var express = require('express');
var router = express.Router();
var axios = require('axios');
var _ = require('lodash');


global.cryptos = null

setInterval(async () => {
  await fetchCryptos()
}, 900000);


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/cryptos', async function(req, res, next) {
  if (cryptos == null) {
    await fetchCryptos()
  }

  res.send({cryptos: cryptos})
})

async function fetchCryptos() {
  const url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?CMC_PRO_API_KEY=4dc39468-fbad-4dcd-857d-ee6641305105';
  const symbols = ['BTC','BCC','XRP','ETH','PHX','XLM','BAT','EOS','PAX','ADA','TRX','BNB','INS','QKC','LTC','RVN','DOCK','TUSD','VET','ARN','NEO','ZEC','XMR','WINGS','ZRX','FUN','ETC','GVT','ICX','MIOTA','ZIL','QTUM','DASH','OST','MFT','KEY','ONT','NANO','IOTX','AE','OMG','DLT','BTG','ELF','WAN','XVG','GO','CDT','RCN','NCASH','AGI','WTC','CMT','HOT','SNM','NPXS','CLOAK','XEM','QLC','LINK','POLY','LSK','ZEN','MDA','AION','STORM','OAX','VIBE','SC','IOST','POA','LEND','STEEM','VIB','POE','BCPT','EVX','KNC','LOOM','CVC','WPR','CND','DENT','BCD','WABI','DNT','STRAT','RDN','ARDR','NAS','FUEL','LRC','AST','GTO','DCR','BLZ','MANA','SALT','KMD','BRD','AMB','TNB','PIVX','POWR','APPC','LUN','SKY','DATA','REQ','NEBL','HC','BTS','PPT','MOD','NAV','MCO','ENG','MTL','QSP','REP','NULS','GNT','MTH','EDO','TNT','THETA','SUB','SNGLS','RLC','GAS','ADX','ENJ','VIA','GRS','SYS','SNT','WAVES','BNT','ARK','XZC','NXS','STORJ','DGD','GXS','ICN','CHAT']

  try {
    let result = await axios.get(`${url}&symbol=${symbols.join()}`);
    cryptos = result.data.data;

    cryptos = _.map(cryptos, (value, key) => {
      return {name: value.name, symbol: value.symbol, market_cap: Math.trunc(value.quote.USD.market_cap)};
    })
  
    cryptos = _.sortBy(cryptos, ['market_cap'])
  
    let index = 0
    cryptos = _.forEach(cryptos, (value, key) => {
      index = index + 1
      return value.row = index
    })
  } catch (error) {
    console.log(`Error: ${error.response.status}: ${error.response.statusText}`)
  }
}


module.exports = router;
